interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];

let currentScore:number = 0
const divEX2: HTMLDivElement = document.getElementById('exercise2') as HTMLDivElement
const score = document.createElement('div')
let showScore = document.createTextNode('Current Score: ' + currentScore + '/10')
score.appendChild(showScore)
divEX2.appendChild(score)

for (const question of questions) {
    const form = document.createElement("form")
    const divQues = document.createElement('div')
    const result = document.createElement("p")
    const divChoices = document.createElement('div')

    const questionP = document.createElement("p")
    questionP.innerText = question.question
    //Create button
    const submitBtn = document.createElement('button')
    submitBtn.textContent = 'Submit'
    //Add choice
    const radios:HTMLInputElement[] = []
    for (const [index, choice] of question.choices.entries()) {
        const div = document.createElement('div')
        const input = document.createElement("input")
        input.type = "radio"
        input.name = "radioChoice"
        input.value = index.toString()

        const span = document.createElement("span")
        span.appendChild(document.createTextNode(choice))

        div.appendChild(input)
        div.appendChild(span)
        divChoices.appendChild(div)

        radios.push(input)

        submitBtn.addEventListener("click", function () {
            const selectedAnswer = radios.find((radio) => radio.checked)
            if(selectedAnswer)
                input.disabled = true
        });
    }
    divQues.appendChild(questionP)
    divQues.appendChild(divChoices)
    divQues.appendChild(submitBtn)
    divQues.appendChild(result)
    form.appendChild(divQues)
    divEX2.appendChild(form)

    submitBtn.addEventListener("click", function (events) {
        events.preventDefault()
        const selectedAnswer = radios.find((radio) => radio.checked)

        if (selectedAnswer) {
            const selectedChoice:number = parseInt(selectedAnswer.value)
            if(selectedChoice === question.correctAnswer){
                currentScore++
                score.innerHTML = 'Current Score: ' + currentScore + '/10'
                result.innerText = 'Correct!'
            }else{
                result.innerText = 'Inorrect!'
            }

        }else{
            alert("please choose an answer first!")
            return 
        }
        submitBtn.disabled = true
    });
}